import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './apps/users/users.module';
import { DBModule } from './db/mongo.db';
import { AccountsModule } from './apps/accounts/accounts.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    AccountsModule,
    UsersModule,
    DBModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
