import { HttpStatus } from '@nestjs/common';
import { Response } from 'express';
import { pick } from 'lodash';
import { Types } from 'mongoose';

const swapStringToObjectId = (id: string): Types.ObjectId => {
  return new Types.ObjectId(id);
};

type InfoData = {
  object: any;
  fields: string[];
};

const getInfoData = ({ object = {}, fields = [] }: InfoData) => {
  return pick(object, fields);
};

const responseData = (resp: Response, http: HttpStatus, data: any) => {
  return resp.status(200).json({ data: data, statusCode: http });
};

export { swapStringToObjectId, getInfoData, responseData };
