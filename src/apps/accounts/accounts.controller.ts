import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  HttpStatus,
  Res,
  UseGuards,
  Req,
} from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { CreateAccountDto } from './dto/create-account.dto';
import { Request, Response } from 'express';
import { responseData } from 'src/utils';
import { AccessTokenGuard } from './guards/accessToken.guard';

@Controller('accounts')
export class AccountsController {
  constructor(private readonly accountsService: AccountsService) {}

  @Post('/sign-up')
  async signUp(
    @Body() createAccountDto: CreateAccountDto,
    @Res() resp: Response,
  ) {
    const account = await this.accountsService.signUp(createAccountDto);
    responseData(resp, HttpStatus.CREATED, account);
  }

  @Post('/sign-in')
  async signIn(
    @Body() createAccountDto: CreateAccountDto,
    @Res() resp: Response,
  ) {
    const tokens = await this.accountsService.signIn(createAccountDto);
    resp.cookie('refreshToken', tokens.refreshToken, {
      httpOnly: true,
      secure: true,
      sameSite: 'strict',
    });
    responseData(resp, HttpStatus.OK, tokens);
  }

  @Get('/logout/:id')
  async logout(@Param('id') id: string, @Res() resp: Response) {
    const logout = await this.accountsService.logout(id);
    resp.cookie('refreshToken', '', {
      httpOnly: true,
      secure: true,
      sameSite: 'strict',
      expires: new Date(0),
    });
    responseData(resp, HttpStatus.OK, logout);
  }

  @Post('/refresh-token')
  async refreshToken(
    @Req() req: Request,
    @Body() { refreshToken },
    @Res() resp: Response,
  ) {
    // console.log(req.cookies.refreshToken);
    const tokens = await this.accountsService.refreshToken(refreshToken);
    responseData(resp, HttpStatus.OK, tokens);
  }

  @UseGuards(AccessTokenGuard)
  @Get(':id')
  async findOne(@Param('id') id: string, @Res() resp: Response) {
    const accountById = await this.accountsService.findOne(id);
    responseData(resp, HttpStatus.OK, accountById);
  }
}
