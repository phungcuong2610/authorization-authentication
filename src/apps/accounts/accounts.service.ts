import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateAccountDto } from './dto/create-account.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Account } from './entities/account.entity';
import { Model, Types } from 'mongoose';
import { getInfoData, swapStringToObjectId } from 'src/utils';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AccountsService {
  constructor(
    @InjectModel(Account.name) private accountModel: Model<Account>,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async signUp(createAccountDto: CreateAccountDto): Promise<Object> {
    const accountByEmail = await this.accountModel
      .findOne({
        email: createAccountDto.email,
      })
      .exec();

    if (accountByEmail) {
      throw new HttpException('Account Is Exists', HttpStatus.NOT_FOUND);
    }

    const hashPassword = await this.hashPassword(createAccountDto.password);

    const newAccount = await this.accountModel.create({
      email: createAccountDto.email,
      password: hashPassword,
      role: createAccountDto.role,
    });
    await newAccount.save();
    return getInfoData({
      object: newAccount,
      fields: ['_id', 'email', 'role'],
    });
  }

  async signIn(accountDto: CreateAccountDto): Promise<{
    id: Types.ObjectId;
    email: string;
    accessToken: string;
    refreshToken: string;
  }> {
    const accountByEmail = await this.accountModel.findOne({
      email: accountDto.email,
    });

    if (!accountByEmail) {
      throw new HttpException('Account Is Not Exists', HttpStatus.NOT_FOUND);
    }

    const checkPassword = bcrypt.compareSync(
      accountDto.password,
      accountByEmail.password,
    );

    if (!checkPassword) {
      throw new HttpException('Password is not correct', HttpStatus.NOT_FOUND);
    }

    const payload = { id: accountByEmail._id, email: accountByEmail.email };
    const tokens = await this.generateToken(payload);
    await this.accountModel
      .findByIdAndUpdate(accountByEmail._id, {
        refresh_token: tokens.refreshToken,
      })
      .exec();

    return {
      id: accountByEmail._id,
      email: accountByEmail.email,
      accessToken: tokens.accessToken,
      refreshToken: tokens.refreshToken,
    };
  }

  async findAll(): Promise<Object[]> {
    const listAccount = await this.accountModel.find().exec();
    return listAccount.map((acc: Account, index: number) => {
      return getInfoData({
        object: acc,
        fields: ['_id', 'email', 'role'],
      });
    });
  }

  async findOne(id: string): Promise<Object> {
    const accountById = await this.accountModel.findById(
      swapStringToObjectId(id),
    );
    return getInfoData({
      object: accountById,
      fields: ['_id', 'email', 'role'],
    });
  }

  async logout(id: string) {
    const updateAccount = await this.accountModel.findByIdAndUpdate(id, {
      refresh_token: '',
    });
    return updateAccount;
  }

  async refreshToken(refreshToken: string): Promise<any> {
    try {
      const verify = await this.jwtService.verifyAsync(refreshToken, {
        secret: this.configService.get('JWT_REFRESH_SECRET'),
      });
      const checkExistToken = await this.accountModel.findOne({
        email: verify.email,
        refresh_token: refreshToken,
      });
      if (checkExistToken) {
        return this.generateToken({
          id: swapStringToObjectId(verify.id),
          email: verify.email,
        });
      } else {
        throw new HttpException(
          'Refresh token is not valid',
          HttpStatus.BAD_REQUEST,
        );
      }
    } catch (error) {
      throw new HttpException(
        'Refresh token is not valid',
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  private async generateToken(payload: {
    id: Types.ObjectId;
    email: string;
  }): Promise<{ accessToken: string; refreshToken: string }> {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('JWT_ACCESS_SECRET'),
        expiresIn: '10s',
      }),
      this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('JWT_REFRESH_SECRET'),
        expiresIn: '7d',
      }),
    ]);

    return { accessToken, refreshToken };
  }

  private async hashPassword(password: string): Promise<string> {
    const saltRound = 10;
    const salt = await bcrypt.genSalt(saltRound);
    const hashPass = await bcrypt.hash(password, salt);
    return hashPass;
  }
}
