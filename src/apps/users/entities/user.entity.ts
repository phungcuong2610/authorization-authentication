import { Account } from './../../accounts/entities/account.entity';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Schema({ timestamps: true })
export class User {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Account' })
  account: Account;

  @Prop()
  fullname: string;

  @Prop()
  age: number;
}

export const UserSchema = SchemaFactory.createForClass(User);
