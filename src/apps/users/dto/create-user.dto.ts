import { IsNotEmpty } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  accountId: string;

  @IsNotEmpty()
  fullname: string;

  @IsNotEmpty()
  age: number;
}
