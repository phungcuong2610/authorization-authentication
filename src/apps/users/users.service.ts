import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './entities/user.entity';
import { Model } from 'mongoose';
import { swapStringToObjectId } from 'src/utils';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}

  async create(createUserDto: CreateUserDto): Promise<User> {
    const userByAccId = await this.userModel
      .findOne({
        account: swapStringToObjectId(createUserDto.accountId),
      })
      .exec();

    if (userByAccId)
      throw new HttpException('User is Exists', HttpStatus.NOT_FOUND);

    const user = await this.userModel.create({
      account: swapStringToObjectId(createUserDto.accountId),
      fullname: createUserDto.fullname,
      age: createUserDto.age,
    });
    await user.save();
    return user;
  }

  async findOne(id: string) {
    const userInfo = await this.userModel
      .aggregate([
        {
          $match: {
            account: swapStringToObjectId(id),
          },
        },
        {
          $lookup: {
            from: 'accounts',
            localField: 'account',
            foreignField: '_id',
            as: 'accountInfo',
          },
        },
        { $unwind: '$accountInfo' },
        {
          $project: {
            _id: 1,
            fullname: 1,
            age: 1,
            'accountInfo.email': 1,
          },
        },
      ])
      .exec();

    if (!userInfo.length)
      throw new HttpException('User Not Exists', HttpStatus.NOT_FOUND);

    return userInfo;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    const updateUser = await this.userModel
      .findOneAndUpdate(
        { account: swapStringToObjectId(id) },
        { fullname: updateUserDto.fullname, age: updateUserDto.age },
      )
      .exec();
    return updateUser;
  }
}
