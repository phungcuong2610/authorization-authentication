import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Res,
  HttpStatus,
  UseGuards,
  Put,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Response } from 'express';
import { responseData } from 'src/utils';
import { AccessTokenGuard } from '../accounts/guards/accessToken.guard';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(AccessTokenGuard)
  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Res() resp: Response) {
    const user = await this.usersService.create(createUserDto);
    responseData(resp, HttpStatus.CREATED, user);
  }

  @UseGuards(AccessTokenGuard)
  @Get(':id')
  async findOne(@Param('id') id: string, @Res() resp: Response) {
    const userInfo = await this.usersService.findOne(id);
    responseData(resp, HttpStatus.OK, userInfo);
  }

  @UseGuards(AccessTokenGuard)
  @Put(':id')
  async update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
    @Res() resp: Response,
  ) {
    const updateUser = await this.usersService.update(id, updateUserDto);
    responseData(resp, HttpStatus.OK, updateUser);
  }
}
