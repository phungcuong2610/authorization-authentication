FROM node:18-alpine

RUN mkdir /authorization-authentication-be

WORKDIR /authorization-authentication-be

COPY package*.json yarn*.lock ./

RUN yarn install --frozen-lockfile

COPY . .

RUN yarn

RUN yarn build 

CMD [ "yarn", "start:dev" ]